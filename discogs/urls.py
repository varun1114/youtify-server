from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', include('website.urls')),
    # url(r'^blog/', include('blog.urls')),
    url(r'^home/', include('website.urls', namespace='website', app_name='website')),
    url(r'^admin/', include(admin.site.urls)),
   	url('', include('social.apps.django_app.urls', namespace='social')),
)
